if [ -z "$1" ];
    then VERSION="0"
    else VERSION=$1
fi

mkdir -p  output/$VERSION/

python3 main$VERSION.py < input/a.in > output/$VERSION/a.out
python3 main$VERSION.py < input/b.in > output/$VERSION/b.out
python3 main$VERSION.py < input/c.in > output/$VERSION/c.out
python3 main$VERSION.py < input/d.in > output/$VERSION/d.out
"""
     @issue: Manipulate input
     @title: Manipulate input
     @description:
     Accept and assign input according to question
     @label: programming, test
     @priority: high
"""

if __name__ == '__main__':
    a = input()
    print(a)
